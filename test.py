# importing dummy functions
from main import app, key, jwt_data
from unittest import TestCase, main, TextTestRunner
import json
import jwt


class Tests(TestCase):
    def setUp(self):
        self.app = app.test_client()

    # test for /login
    def test_login(self):
        response = self.app.post('/login', data = json.dumps({"login": "test","password": "test"}), content_type = 'application/json')
        data = response.get_json()
        # using dummy function
        passed = (data['data']['message'] == jwt.encode(json.loads(jwt_data("test", "test"))['data'], key, algorithm = 'HS256').decode('utf-8'))
        assert passed

    # test for /cashbox/client (input: phone)
    def test_cashbox_client_phone(self):
        response = self.app.get('/cashbox/client', query_string = {'clientid':'+7927864327'}, follow_redirects = True, headers = {'Authorization': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjYXNoYm94X2lkIjoiNjc4NDY1MyIsInBhcnRuZXJfaWQiOiI3MzQ4NjczNDgiLCJ1c2VyX2lkIjoiMzI0MzI1In0.kNyJm8gXyNBPxUCYh_sA4THF9-ok9YV7kja1I0YGKs8'})
        data = response.get_json()
        expected = {
            "data": {
                "balance_cashback": 550, 
                "code": 7777, 
                "discount": 3.3, 
                "first_name": "Rafael", 
                "last_name": "Shamil", 
                "percentage_cashback": 90, 
                "serial_number": "5"
            }, 
            "status": "Ok"
        }
        passed = (json.dumps(data) == json.dumps(expected))
        assert passed

    # test for /cashbox/client (input: serial_number)
    def test_cashbox_client_serial_number(self):
        response = self.app.get('/cashbox/client', query_string = {'clientid':'7927864327'}, follow_redirects = True, headers = {'Authorization': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjYXNoYm94X2lkIjoiNjc4NDY1MyIsInBhcnRuZXJfaWQiOiI3MzQ4NjczNDgiLCJ1c2VyX2lkIjoiMzI0MzI1In0.kNyJm8gXyNBPxUCYh_sA4THF9-ok9YV7kja1I0YGKs8'})
        data = response.get_json()
        expected = {
            "data": {
                "balance_cashback": 550, 
                "code": 7777, 
                "discount": 3.3, 
                "first_name": "Rafael", 
                "last_name": "Shamil", 
                "percentage_cashback": 90, 
                "serial_number": 5
            }, 
            "status": "Ok"
        }
        passed = (json.dumps(data) == json.dumps(expected))
        assert passed

    # test for /cashbox/client (input: barcode)
    def test_cashbox_client_barcode(self):
        response = self.app.get('/cashbox/client', query_string = {'clientid':'https://wollzy.com/pass/7927864327'}, follow_redirects = True, headers = {'Authorization': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjYXNoYm94X2lkIjoiNjc4NDY1MyIsInBhcnRuZXJfaWQiOiI3MzQ4NjczNDgiLCJ1c2VyX2lkIjoiMzI0MzI1In0.kNyJm8gXyNBPxUCYh_sA4THF9-ok9YV7kja1I0YGKs8'})
        data = response.get_json()
        expected = {
            "data": {
                "balance_cashback": 550, 
                "code": 7777, 
                "discount": 3.3, 
                "first_name": "Rafael", 
                "last_name": "Shamil", 
                "percentage_cashback": 90, 
                "serial_number": 5
            }, 
            "status": "Ok"
        }
        passed = (json.dumps(data) == json.dumps(expected))
        assert passed

    # test for /cashbox/check
    def test_cashbox_check(self):
        # response = self.app.get('/cashbox/check', query_string = {'clientid':'https://wollzy.com/pass/7927864327'}, follow_redirects = True, headers = {'Authorization': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjYXNoYm94X2lkIjoiNjc4NDY1MyIsInBhcnRuZXJfaWQiOiI3MzQ4NjczNDgiLCJ1c2VyX2lkIjoiMzI0MzI1In0.kNyJm8gXyNBPxUCYh_sA4THF9-ok9YV7kja1I0YGKs8'})
        response = self.app.post('/cashbox/check', data = json.dumps({"clientid": "https://ortezgroup.com/pass/95c9d8", "check_id": "123", "total": 300.5, "used_balance": 100.5, "code": 555777, "object": {}, "serial_number": "jh89j89"}), content_type = 'application/json')
        data = response.get_json()
        print(data)
        expected = {
            "data": {
                "message": "Data has been saved"
            },
            "status": "Ok"
        }
        passed = (json.dumps(data) == json.dumps(expected))
        assert passed

if __name__ == '__main__':
    log_file = 'test_log.txt'
    f = open(log_file, "w")
    runner = TextTestRunner(f)
    main(testRunner=runner)
    f.close()