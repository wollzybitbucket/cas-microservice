from config import key, balance_host, balance_port

import json
import jwt
import requests
from threading import Thread

from flask import Flask, request, jsonify, url_for
from flask_api import status


app = Flask(__name__)
url = 'http://'+balance_host + ':' + str(balance_port)


def get_tablepcs_serial_number_id(partner_id, client_id):
    response = requests.get(url + '/tablepcs/serialnumber', params={'partner_id':partner_id, 'client_id':client_id})
    return response.json()


def get_tablepcs_client_id(partner_id, serial_number_id):
    response = requests.get(url + '/tablepcs/client', params={'partner_id':partner_id, 'serial_number_id':serial_number_id})
    return response.json()


def get_client(client_data):
    response = requests.get(url + '/client', params=client_data)
    return response.json()


def loyalty_property(data):
    response = requests.get(url + '/loyalty/property', params=data)
    return response.json()


def loyalty_check_property(data):
    response = requests.get(url + '/loyalty/check/property', params=data)
    return response.json()

def save_to_push(data):
    response = requests.post(url + '/push', json=data)
    return response.json()


def serialnumber_id(*args):
    return {
        'status': 'Ok',
        'data': {
            'id': 1
        }
    }

# dummy function:
def jwt_data(param1, param2):
    return {
        'status': 'Ok',
        'data': {
            'cashbox_id': '6784653',
            'partner_id': '734867348',
            'user_id': '324325'
        }
    }


def make_error(code, message):
    err = {
        'error': {
            'code': code,
            'message': message
        },
        'status': 'Error',
        'data': ''
    }
    return jsonify(err), code


class GetProperty(Thread):

    def __init__(self, propert, serial_number_id):
        Thread.__init__(self)
        self.propert = propert
        self.serial_number_id = serial_number_id
    
    def run(self):
        propert_data = {'serial_number_id': self.serial_number_id}
        # request to a dummy route, more exact command is down below:
        # outp = requests.get(<route to /loyalty/property>, params = propert_data).json()
        outp = loyalty_property(propert_data)
        if outp['status'] != 'Ok':
            return make_error('400', 'Client not found')
        self.propert.append(outp)


@app.route('/login', methods = ['POST'])
def login():
    
    req = request.json
    # request to a dummy function:
    db_response = jwt_data(req['login'], req['password'])
    if db_response['status'] != 'Ok':
        return make_error('400', 'Authorization error')
    # structure of db output is assumed
    access_token = jwt.encode(db_response['data'], key, algorithm = 'HS256').decode('utf-8')
    resp = {
        'satatus': 'Ok',
        'data': {
            'message': access_token
        }
    }
    return jsonify(resp)


@app.route('/cashbox/client', methods = ['GET'])
def cashbox_client():
    if not 'Authorization' in request.headers.keys():
        return make_error('403', 'Forbidden')
    clientid = request.args.get('clientid')
    cli = []
    propert = []
    serial_number_id = ''
    if clientid[0] == '+':
        client_data = {'phone': clientid}
        cli = get_client(client_data)
        if cli['status'] != 'Ok':
            return make_error('400', 'Client not found')
        client_id = cli['data']['id']
        # structure of jwt content is assumed
        partner_id = jwt.decode(request.headers.get('Authorization'), key, 'utf-8', algorithms = ['HS256'])['partner_id']
        
        tablepcs_outp = get_tablepcs_serial_number_id(partner_id, client_id)
        if tablepcs_outp['status'] != 'Ok':
            return jsonify(tablepcs_outp)
        serial_number_id = tablepcs_outp['data']['id']
        propert_data = {'serial_number_id': serial_number_id}
        propert.append(loyalty_property(propert_data))
        if propert[0]['status'] != 'Ok':
            return make_error('400', 'Client not found')
    else:
        if '/' in clientid:
            clientid = clientid.split('/')[-1]
        serial_number_data = {'serial_number': clientid}
        serial_number = serialnumber_id(serial_number_data)
        if serial_number['status'] != 'Ok':
            return make_error('400', 'Client not found')
        serial_number_id = serial_number['data']['id']
        get_property = GetProperty(propert, serial_number_id)
        get_property.daemon = True
        get_property.start()
        # structure of jwt content is assumed
        partner_id = jwt.decode(request.headers.get('Authorization'), key, 'utf-8', algorithms = ['HS256'])['partner_id']
        
        tablepcs_outp = get_tablepcs_client_id(partner_id, serial_number_id)
        if tablepcs_outp['status'] != 'Ok':
            return jsonify(tablepcs_outp)
        client_id = tablepcs_outp['data']['client_id']
        client_data = {'serial_number_id': serial_number_id}

        cli = get_client({'id': client_id})
        if cli['status'] != 'Ok':
            return make_error('400', 'Client not found')
    outp = {
        'status': 'Ok',
        'data': {
            'first_name': cli['data']['first_name'],
            'last_name': cli['data']['last_name'],
            'discount': propert[0]['data']['discount'],
            'percentage_cashback': propert[0]['data']['percentage_cashback'],
            'balance_cashback': propert[0]['data']['balance_cashback'],
            'serial_number': serial_number_id,
            'code': 7777
        }
    }
    return jsonify(outp)


@app.route('/cashbox/check', methods = ['POST'])
def cashbox_check():
    inp = request.get_json()
    serial_number_id = inp['serial_number']
    propert_data = {
        'data': {
            'serial_number_id': serial_number_id,
            'check': inp['object']
        }
    }

    propert = loyalty_check_property(propert_data)
    push_data = {
        'data': {
            'serial_number_id': serial_number_id,
            'property': propert
        }
    }
    response = save_to_push(push_data)
    if  not 'Ok' in response['status']:
        return make_error(400, 'Can`t save to push.')
    outp = {
        'status': 'Ok',
        'data': {
            'message': 'Data has been saved'
        }
    }
    return jsonify(outp)


if __name__ == '__main__':
    app.run(host = '0.0.0.0', debug = True)