FROM python:3-onbuild

RUN mkdir code

ADD . /code

WORKDIR /code

EXPOSE 5000

CMD ["python", "main.py"]